it('should add new item on add-item button click', () => {
  cy.visit('/');
  cy.contains('Happy Todo Application');
  cy.get('input').type('buy some milk'); 
  cy.get('button').click();
  cy.get('ul').find('div').find('li').its('length').should('eq', 1)
});
