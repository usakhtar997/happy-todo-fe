import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  // Array maintaining todo list items 
  allItems = new Array();
  todoService: TodoService;
  constructor(todoService: TodoService) {
    this.todoService = todoService;

   }

  //  Get items list when application starts
  ngOnInit(): void {
    this.todoService.getTodoItems().subscribe(t => {
      if(t.length > 0){
        t.forEach(item => {
          if(item.length !=0) {
            this.allItems.push(item.toString());
          }
        });
    }
    });
  }

  //  Add new item
  addItem(value:string){
    this.todoService.addItem(value).subscribe(t => {
      this.allItems.push(value);
    });
  }
}
