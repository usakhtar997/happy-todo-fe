import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class TodoService {
  baseURL: string = environment.BASE_URL;

  constructor(private http: HttpClient) {
  }
 
  public getTodoItems(): Observable<String[]> {
    return this.http.get<String[]>(this.baseURL + 'todos');
  }

  public addItem(item : String): Observable<any> {
    let itemMap = new Map();  
    itemMap.set('item', item);     
    const headers = {'content-type': 'application/json'}
    const body = Object.fromEntries(itemMap);
    console.log(body)
    return this.http.post(this.baseURL + 'todos', body,{'headers':headers});
  }
}
