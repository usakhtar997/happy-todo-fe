import { TestBed, inject } from '@angular/core/testing';
import { TodoService } from './todo.service';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';

describe('TodoService', () => {
  let service: TodoService

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TodoService],
      imports: [HttpClientModule]
    })
    service = TestBed.get(TodoService);
});

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it("should return data", done => {
    service.getTodoItems().subscribe(t => {
      expect(t[0]).toEqual("Buy some milk")
      done()
    });
  });

  it("should add new item", done => {
    service.addItem("Buy soda").subscribe(t => {
      done()
    },(error: HttpErrorResponse) => {
      expect(error).not.toBeNull()
  });
  });
});
