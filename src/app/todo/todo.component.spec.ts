import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TodoService } from './todo.service';
import { TodoComponent } from './todo.component';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
describe('TodoComponent', () => {
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoComponent ],
      providers: [TodoService],
      imports: [HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create todo component', () => {
    expect(component).toBeTruthy();
  });

  it('should create todo empty list', () => {
    const itemList = fixture.debugElement.nativeElement.querySelector('ul');
    expect(itemList).toBeTruthy();
  });

  it('should create add-item button', () => {
    const itemList = fixture.debugElement.nativeElement.querySelector('button');
    expect(itemList).toBeTruthy();
  });

  it('should create item input field', () => {
    const itemList = fixture.debugElement.nativeElement.querySelector('input');
    expect(itemList).toBeTruthy();
  });
});
