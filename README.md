# Happy ToDo Application FrontEnd

Happy Todo is a simple todo application to demonstrate how to build application in angular using A-TDD approach.

## Acceptance Test-Driven Development (A-TDD)
The application is build following A-TDD approach, while developing project acceptance test was first written in accordance with apiblueprint to ensure contract.After that unit tests were written followed by their implementation.

## Program Structure
![Program Structure](https://i.ibb.co/MsQvZJr/Screenshot-2021-11-18-at-11-57-16-PM.png)

## Installation

Use command 

`npm install` 

to retrieve all the packages being used.

## Build

Application can be build simply by executing:

`npm run build` or `ng build`

## Running
Server can be started by simply just executing command

`npm run start` or `ng serve`

By default application starts on port `4200`.

## Test

Packages used to perform testing are 
 
- Cypress
- Apiary
- Karma
- Jasmine

As application is built focusing on A-TDD. So acceptance and contract tests are performed by using apiblueprint tool called [https://apiary.io/](https://apiary.io/). 

The apiblueprint being used is

![apiary](https://i.ibb.co/7VS37Yw/apiary.png)

and can be accessed by public url
[http://private-a96d5-todo150.apiary-mock.com/todos](http://private-a96d5-todo150.apiary-mock.com/todos)

The application uses Cypress for acceptance testing

![cypress](https://i.ibb.co/MhGj2vD/Screenshot-2021-11-19-at-12-12-01-AM.png)
 
in order to run acceptance test using Cypress use command

`npm run cy:test`

In order to run unit tests simply execute command

`npm run test` or `ng test`

## Deployment

#### Docker

The application image can be created using docker as

`docker build -t happy-todo-fe .`

and then to run container

`docker run -p 80:80 happy-todo-fe`

#### CI|CD

The CI|CD pipeline has four stages 

![pipeline](https://i.ibb.co/V2THZth/Screenshot-2021-11-19-at-12-05-52-AM.png)

##### test
The goal of this stage is to perform testing.

##### dockerize
The goal of this stage is to create docker image of an app and deploy it on Elastic Container Registry (ECR).

Tools required to complete this stage are
- Docker
- Elastic Container Registry
- awscli
- eksctl

The ECR repositry being used is:

![repositry](https://i.ibb.co/TP4BRwD/Screenshot-2021-11-19-at-12-07-04-AM.png) 

The repositry can be created using AWS Cli using command

`aws ecr create-repository --repository-name happy-todo-fe --region ap-southeast-1`

##### deploy
The goal of this stage is to deploy docker image from Elastic Container Registry (ECR) to Amazon Elastic Kubernetes.

Tools required to complete this stage are
- Amazon Elastic Kubernetes
- awscli
- eksctl

Before this stage we have to make sure that cluster is properly configured. 
To configure cluster properly we need to perform some 3 steps

1. Create cluster

The cluster can be created by eksctl simply as

`eksctl create cluster --name happy-todo-fe-cluster --region ap-southeast-1 --fargate`

![cluster](https://i.ibb.co/7C0CdN7/Screenshot-2021-11-19-at-12-08-38-AM.png) 

2. Deploy

Deployment can be done using command

`kubectl apply -f deployment.yaml`

after the successfull execution of above command pod will be started.

3. Start service

Service can be started using command

`kubectl apply -f service.yaml`






